# ISTAT Framework

Placeholder for orchestrating the **ISTAT framework**, including the **Meta and Data Manager** application, within the scope of the SIS-CC Community.  
The source code is hosted by Eurostat (credentials are required): https://citnet.tech.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/istat_data_browser/browse
