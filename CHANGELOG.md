RELEASE NOTES - 03-07-2020

MDM v.1.2.0 (.NET CORE 3.1.0)  
Release Date: 3 July 2020  
DDB v.1.3  
MASTORE v.6.9  
AUTHDB v.1.1  
RMDB v.1.2  
MA WS v.1.26.1 (.NET CORE 3.1.0)  
NSI WS v.7.12.1 (.NET CORE 3.1.0)  

The following bugs/improvements have been covered:
- Fix on security (reported from version 1.0.1_SECURITY)
- Fix on long filters
- Fix on deselecting dimension in Dataflow Builder: not allowed if it is part of a group
- ISTAT-5 Various fixes on annotations:
	- management MM side of the annotations
	- zoom on all text fields 
	- add multiple attached data files
	- multilingual for metadata URL, attached data files, dataflow notes
	- add format for URL
	- ISTAT-6: text truncated in 'Attached data file'
- ISTAT-13 Modified default configuration for ORDER annotation, default agencies and parameters for pagination
- ISTAT-55 id and type are not mandatory for annotations any more
- ISTAT-56 message on loader's report stating only the first error for each row is shown
- ISTAT-60 Mapping sets are now orderrf from the oldest to the newest
- ISTAT-74 bug fix for creating a content-constraint from a DSD without attributes
- ISTAT-85 added permission to manage 'work annotation'
- ISTAT-94 Check on coded observation values during loading
- ISTAT-95 added annotation for categorisations' order
- ISTAT-96 Support for multirow table
- ISTAT-98 support for new version of NSI WS and WS implemented in .Net Core 3.1
- ISTAT-99 Support for architectural scenario whith read/write access to MDM without installing a DDB/RMDB
- ISTAT-99 Support for installing both MSDB and DDB on the same database
- ISTAT-99 Support for having Oracle MSDB
- ISTAT-103 Difference in number of codelists' item in Simple and Advanced Filter
- ISTAT-104 Fix on download codelist CL_SERIES in csv format
- ISTAT-166 Various improvements on layout annotations
- ISTAT-186 Bug on empty codes in Advanced Filters (Dataflow Builder)

DCAT and Referential Metadata:

Bugs solved:
- Corrupted images in the DCAT-AP_EN catalog
- The HTML editor does not remember whether a field was compiled in HTML or flat text mode
- The deletion of a field affects the compilation of the others (it doesn't let you fill in anything anymore)
- In the attributes tree you lose values when you change language or settings
- In the DCAT report (HTML widget) there is not all the information in the package_show 

small improvements:
- Edited some MSD labels
- Added the "CL_" prefix for Codelist in the DCAT MSD (MSD)
- Removed complete URIs from the interface
- Added all licenses in the controlled vocabulary: https://github.com/italia/daf-ontologie-vocabolari-controllati/blob/master/VocabolariControllati/licences/licences.csv
- Removed the "DCAT_AP_LICENSE_TYPE" attribute from the MSD, which is no longer referenced
- Added (sequential) numbers to attributes in reports

New features:
- New architecture support (the HTML widget has API Metadata as its only dependency and structural metadata is retrieved from API Metadata via API Node; a cache has also been introduced, both on structural and referential metadata)

-----------------History of previous releases----------------

MDM v.1.1.0 (.NET CORE 2.2.5)  
Release Date: 19 May 2020  
DDB v.1.2  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.2  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
- new management of Dataset level attributes
- Loader: added check for coherence of attributes in Filt table
- loading: delete FactS_TEMP table if empty
- fix on CatDataflow > 500 char
- Modified layout annotation (DM side) and added new ones
- Added the possibility to use '-' char in IDs
- Blocking error for conflict at attribute level
- Added items' codes in Dataflow Builder - Filters (advanced mode)
- Fix on export of artefacts with referenced organization schemes: “Organisation Schemes can not be set to final”
- Support for Trusted Connection
- MM: implementation of Hierarchical Codelist (only at stub level)

 ---------------

MDM v.1.0.1 (.NET CORE 2.2.5)  
Release Date: 20 April 2020  
DDB v.1.2  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.2  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
- fix on creating a filter on a coded attribute with no values available in the cube
 
Fixes for DCAT and Referential Metadata:
 - The validity check for reportingBegin - reportingEnd fields has been added
 - All attributes of type "String" can be modified through the XHTML editor, with the possibility to switch to classic text (with zoom functionality)
 - Added the possibility to zoom on the name of the MetadataSet
 - All String and XHTML attributes are by default multilingual (the current annotation is ignored)
 - If a report is exposed via API it will be read-only
 - Removed the ability to add categories from the MetadataSet tree
 - IDs from categories and attributes were removed when compiling the report
 - After having saved a report it remains open
 - The deletion of dataflows is not permitted when associated reports are present
 - The HTML editor has been configured to allow the user to directly insert/edit HTML
 - Added the management of the IsPresentational implementation associated to the MSD (checkbox in interface)
 - Problems of slow compilation of attributes have been solved
 - DCAT: catalog report deletion is prevented if dataflow reports are present
 - DCAT: a new version of the MSD has been released
 - DCAT: annotations and attachments have been removed from the compilation of attributes
 
 -----------------

MDM v.1.0.0 (.NET CORE 2.2.5)  
Release Date: 20 March 2020  
DDB v.1.2  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.2  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
 - OBS_VALUE column in FactS table is now of type float instead of real
 - fix bug on blank page on create artefact
 - fix on AssociatedCube annotation when upgrading a cube
 - fix on concurrent uploads management
 - modified default configuration for NSI WS and MA WS (commented configLocation parameter)
 - added new configuration parameter for timeouts of single web services
 - added new applicative error for import of dsds referencing not final artefacts
 - fix download in sdmx formats for very big dataflows
 - fix download in CUSTOM CSV format for very big dataflows
 - Derived ItemScheme: support for creation of hierarchical itemscheme derived not at root level
 - Fixed a bug on creation of dataflows with header template
 - Updated MA WS default configuration for supporting content constraint automatic creation while putting into production a dataflow
 - New management of 'isFinal' field for artefacts in 'Export on remote ws' functionality
 - Minor fix on codelist's items order management
 - security fix on Recover Password functionality
 
Fixes for DCAT and Referential Metadata:
 - It is now possible to save generic referential metadata reports without configuring the DCAT module first
 - Support for OrganisationUnitScheme references in MSDs

-----------------

MDM v.0.6.4 (.NET CORE 2.2.5)  
Release Date: 26 February 2020  
DDB v.1.2  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.2  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
 - Nodes configuration: DM Api url for ping is now case insensitive
 - Fix for DM API WS swagger
 - Modified DM API WS appsettings.json file connection string format
 - 2211 Improvement artefact's saving: popup window is now not closed after saving
 - 2212 Message 'Default category scheme is empty' is now a warning and not an error
 - 2214 Changed "D&M Manager" logo to "M&D Manager"
 - 2215 Added 'Delete cube' button in 'List of cubes'
 - 2216 Custom annotations are now not visible inside the artefact for anonymous users
 - 2217 Compare DSD - fix on Show Details for DSDs from files not present in the MSDB
 - 2218 Names of Custom annotations' tabs are now multilingual
 - 2219 Report Import Sdmx Artefacts - added final attribute for imported artefcats
 - 2220 Dataflow/Metadataflow - Save button disabled if DSD/MSD has not been selected
 - 2221 Added Import button in MSD page
 - 2222 Dataflow/Metadataflow creation: only final DSD/MSD can now be selected as referenced artefacts
 - 2224 Child creation for an item in an itemscheme: now added at the end of the list (and not at the top)
 - 2225 Fix on items' order modification: now the order of ALL the items in the itemscheme is recomputed
 - 2226 Button to assign the current ordering of the items of an itemscheme to all the languages
 - 2228 Save button is now not visible in Derived Itemscheme tab
 - 2229 Order annotation is now NOT saved for category schemes' and concept schemes' items if the artefact is not final
 - 2230 The parameter for the minimum number of nodes in a tree for disactivating 'Expand all' functionality is now managed in configuration
 - 2231 Artefact delete is now permitted also if the user does not have permission on the artefact's agency
 - 2232 Configuration section has been added to Manage Permission page
 - 2233 Fix on Update MSD (a parameter was passed uncorrectly)
 - 2235 Updated management of messages for concurrent updates in the same cube (using a database traffic light)
 - 2236 Search function in tables: interactive search for not-dataset tables and implementation of "contains" predicate
 - Added "Advanced filter" mode in download functionality (for both cubes and dataflows)
 - Fixed bug on import csv file without BOM with special characters 
 - Fix on saving the application configuration
 
 
Fixes for DCAT and Referential Metadata:
 - Enlarged search text input for Metadatasets
 - Added language selection in Report's window
 - Added the possibility to save an incomplete report
 - New management of "isPresentational" annotation
 - Different colouring for mandatory fields
 - Fixed bug on 'not publishable' flag
 - Modified error thrown when dragging categories during Metadatasets' categorisation
 - Added multilingual support for the XHTML editor
 - The functionality for the Sdmx-Json download of a report has been moved among actions for a single report
 - Report's download is now in HTML
 - Added support for links in the HTML WYSIWYG editor
 - Changes to the HTML page to visualize a Metadataset
 - Changes to the HTML page to visualize a Report
 - Fix on the management of numeric fields types
 - Fix on attributes with referenced codelists at the first level of the tree when compiling the report
 - MetadataAttribute Identifier (ID) is now visibile when compiling
 - 2210 The Annotation for stating whether a report's attribute is multilingual is now settable in Node's configuration
 - Presentational attributes management has been improved
 - Created a folder with default configurations
 - Added support for multiple DCAT-AP_IT catalogues in the same node
 - DCAT-AP_IT reports' compiling: the possibilities to set a field as 'not-publishable' and to add attachments are now disabled
 - Fix on License's version: reference to a wrong codelist has been removed
 - Added direct links to APIs, both for Generic Metadatasets and DCAT-AP_IT
 - Added a codelist for languages
 - Mangement of Date and DateTime fields has been differientiated
 - Changed default labels for the side menu

-----------------

MDM v.0.6.3 (.NET CORE 2.2.5)  
Release Date: 7 February 2020  
DDB v.1.1  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.1  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
 - TASK 2234 - Updated table management for not-coded attributes and dimensions
 - Added messages for concurrent updates in the same cube
 - Ping towards MA WS artefacts is now case insensitive
 - Fix ping endpoint with proxy
 - Fix Internet Explorer not loading properly
 
DCAT and Referential Metadata:
 - Manage of 'Is Publishable' field in reports' attributes
 - Manage of attachments for a single attribute of a report
 - Link for downloading an attachment (added a button next to the name)
 - Use of ConceptSchemes for attributes' titles in HTML widgets
 - Added download of reports in SDMX-JSON format
 
----------------------------

MDM v.0.6.2 (.NET CORE 2.2.5)  
Release Date: 8 January 2020  
DDB v.1.1  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.1  
MA WS v.1.24.9 (.NET CORE 2.2.5)  
NSI WS v.7.10.10 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
 - Modified default configuration for MA WS to activate Policy Module by default and resolve security issues
 - Fix for hiding local path in log files (security issue)

----------------------------

MDM v.0.6.1 (.NET CORE 2.2.5)  
Release Date: 20 December 2019  
DDB v.1.1  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.1  
MA WS v.1.24.0 (.NET CORE 2.2.5)  
NSI WS v.7.10.6 (.NET CORE 2.2.5)  

The following bugs/improvements have been covered:
 - Updated message for user's password changement
 - Functionality ‘Compare DSD’ moved under Utility
 - Copy on remote web service: added possibility to copy also referenced artefacts
 - Copy on remote web service: hidden only-read nodes among selectable destination nodes
 - New format for names of files with items in a specific language: fileA_EN.csv
 - Fix on not refreshing some pages after changing application language
 - Filters on table's columns: 
	 - insert box blocked and not shown only on mouse-over
	 - search only after pressing ok button (otherwise too many requests are sent)
	 - fix on deleting search text (some chars were printed again)
 - Dataflow builder: categories are now shown ordered by ORDER annotation and not in alphabetic order
 - Fix in Table Preview after setting a filter on a column for a dataflow that already had a fitler
 - Improved message ‘Category Scheme cannot be empty'
 - Possibility to set agency name in configuration for default agencies
 - Dataflow Builder: added buttons Ok and Cancel for confirming the isnert of a filter
 - Added Istat logo in 'Classification Server' Use Mode
 - Added buttons for Copying or Exporting an artefact on a remote ws in artefacts' list window
 - Derived Item scheme: 
	 - possibility to import a flat itemscheme if 'Add parent' is not checked
	 - possibility to select all children/discendants
	 - 'Select all' now impact only visible items
	 - Already inserted items are now shown in gray and cannot be selected again
	 - Removed unnecessary actions
	 - Added missing translation
	 - Checks now effect 'Add item' action
 - Merge item schemes: 
	 - new item scheme's name is now defined by the user
	 - when conflicts at item level exists a warning is shown: priority is given to the itemscheme on the left
	 - now items that are present in both itemschemes are identified by a specific icon

--------------------------------

MDM v.0.6.0 (.NET CORE 2.2.5)  
Release Date: 29 November 2019  
DDB v.1.1  
MASTORE v.6.7  
AUTHDB v.1.1  
RMDB v.1.1  
MA WS v.1.24.0 (.NET CORE 2.2.5)  
NSI WS v.7.10.6 (.NET CORE 2.2.5)  

The following new features have been impleemnted:
 - Dynamic scroll in tables
 - Welcome Page (displayable or not)
 - Classification Server Use Mode
 - Derived ItemScheme
 - Merged ItemScheme
 - Transcodings creation from Content Constraints
 - Shortcut to DataManager steps
 - Download of dataflows not in production
 - Database Reset

The following bugs/improvements have been covered:
 - Added possibility to test endpoint urls in DM Api Wizard
 - Added scrollbar in Side Menu
 - Artefacts download: added possibility to export IS_DEFAULT column
 - Improvement of drag and drop in tree representation
 - Upgrade DSD
		- added button for downloading a report
		- added possibility tu upload a dsd from file
		- possibility to download Compare DSD report
 - Fix for DSD download in RTF
 - Fix for error SQL_NO_DATA_TO_SAVE for codelist without name in a specific language
 - Fix for opening in Excel csv downloaded from a codelist
 - Fix for codelist CL_SERIES+IAEG-SDGs+1.0
 - Fix ‘Show content constraints’ functionality from dataflows' page
 - Fix for Content Constraints' generation for a node without DM API WS
 - Language Management according to ISO Standard
 - Language Management Improvement
 - Icon homogenization in Builder and File Mapping
 - Improvement of message 'Session Expired' switching from a node to another
 - Fix on search on an items' table after language switch (reamins blocked)
 - Dataflow Builder, Filters in Adavanced Mode: fixed a bug on values for attributes
 - Homogenization between windows for comparing items from 'Compare DSDs' and 'Compare Itemschemes' functionalities
 - Upgrade DSD: added request for user's confirmation
